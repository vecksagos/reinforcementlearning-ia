#ifndef HASHQFUNCTION_H
#define HASHQFUNCTION_H

#include "qFunction.h"
#include <map>
#include "vFunction/hashVFunction.h"
#include "action/actionSet.h"
#include "action/action.h"
#include <string>

class ActionSet;
class State;

class HashQFunction: public QFunction {
private:
  std::map<Action, HashVFunction> table;
  ActionSet *actionSet;
public:
  HashQFunction(ActionSet *actionSet = NULL);
  double getValue(State &state, Action &action);
  double getMaxValue(State &state);
  void setValue(State &state, Action &action, double value);
  std::string to_string();
};

#endif /* HASHQFUNCTION_H */
