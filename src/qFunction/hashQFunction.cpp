#include "hashQFunction.h"
#include "state/state.h"
#include "action/actionSet.h"
#include "vFunction/hashVFunction.h"
#include <random>
#include <limits>
#include <boost/lexical_cast.hpp>

using namespace std;
using boost::lexical_cast;

HashQFunction::HashQFunction(ActionSet *actionSet) :
  actionSet(actionSet) {
}

double HashQFunction::getValue(State &state, Action &action) {
  return table[action].getValue(state);
}

void HashQFunction::setValue(State &state, Action &action, double value) {
  table[action].setValue(state, value);
}

// TODO: it's getting the Q of all possible action, not only the valid action
double HashQFunction::getMaxValue(State &state) {
  double max = numeric_limits<double>::min();
  if (actionSet == NULL) {
    ActionSet *validAction = actionSet->getValidActions(state);
    for (auto action : *validAction)
      max = (max > table[action].getValue(state)) ?
        max : table[action].getValue(state);
  }

  for (auto it = table.begin(); it != table.end(); ++it)
    max = (max > it->second.getValue(state)) ? max : it->second.getValue(state);

  return max;
}

string HashQFunction::to_string() {
  string s = "size: " + lexical_cast<string>(table.size()) + "\n";
  for (auto it = table.begin(); it != table.end(); ++it)
    s += "Action " + it->first.to_string() + " " + it->second.to_string() + "\n";
  return s;
}
