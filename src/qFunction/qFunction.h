#ifndef QFUNTION_H
#define QFUNTION_H

#include <string>

class State;
class Action;

class QFunction {
public:
  virtual double getValue(State &state, Action &action) = 0;
  virtual double getMaxValue(State &state) = 0;
  virtual void setValue(State &state, Action &action, double value) = 0;
  virtual std::string to_string() = 0;
};

#endif /* QFUNTION_H */
