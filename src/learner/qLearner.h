#ifndef QLEARNER_H
#define QLEARNER_H

class QLearner {
private:
  double alpha, gamma;
public:
  void update(State &previousState, Action &previousAction, State &state,
              Action &action, double reward);
}

#endif /* QLEARNER_H */
