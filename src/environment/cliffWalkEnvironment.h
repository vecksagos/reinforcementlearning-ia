#ifndef CLIFFWALKENVIRONMENT_H
#define CLIFFWALKENVIRONMENT_H

#include <vector>
#include <utility>
#include <string>
#include "action/action.h"
#include "action/actionSet.h"
#include "environment.h"
#include "action/actionFilter.h"

class CliffWalkEnvironment: public Environment, public ActionFilter {
private:
  std::vector<std::vector<int>> environment;
  std::vector<std::vector<bool>> traps;
  std::vector<std::vector<bool>> walls;
  /*
    first = height
    second = width
  */
  std::pair<int, int> dimen;
  std::vector<double> player;
  std::vector<double> treasure;

public:
  static Action UP;
  static Action DOWN;
  static Action LEFT;
  static Action RIGHT;
  static ActionSet ACTIONSET;
  const static char TREASURE = 'T';
  const static char PLAYER = '@';
  const static char FLOOR = '.';
  const static char WALL = '#';
  const static char TRAP = '^';
  std::vector<double> initState;
  CliffWalkEnvironment();
  std::string to_string();
  bool isPermitted(State &state, Action &action);
  double doAction(Action &action);
  State getState();
  bool isTerminalState();
  void initRandom();
  void init(State &state);
};

#endif /* CLIFFWALKENVIRONMENT_H */
