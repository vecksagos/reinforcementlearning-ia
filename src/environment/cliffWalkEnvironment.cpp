#include "cliffWalkEnvironment.h"
#include <iostream>
#include <boost/lexical_cast.hpp>

using namespace std;
using boost::lexical_cast;

Action CliffWalkEnvironment::UP({-1,0});
Action CliffWalkEnvironment::DOWN({1,0});
Action CliffWalkEnvironment::LEFT({0,-1});
Action CliffWalkEnvironment::RIGHT({0,1});
ActionSet CliffWalkEnvironment::ACTIONSET({UP, DOWN, LEFT, RIGHT});

CliffWalkEnvironment::CliffWalkEnvironment() {
  CliffWalkEnvironment::ACTIONSET.setFilter(this);
  cin >> dimen.first >> dimen.second;
  pair<double, double> pos;
  cin >> pos.first >> pos.second;
  player = {pos.first,
            pos.second};
  initState = player;
  cin >> pos.first >> pos.second;
  treasure = {pos.first,
              pos.second};

  environment.resize(dimen.first);
  traps.resize(dimen.first);
  walls.resize(dimen.first);
  for (int i = 0; i < dimen.first; ++i) {
    environment[i].resize(dimen.second);
    traps[i].resize(dimen.second);
    walls[i].resize(dimen.second);
    for (int j = 0; j < dimen.second; ++j) {
      environment[i][j] = CliffWalkEnvironment::FLOOR;
      traps[i][j] = false;
      walls[i][j] = false;
    }
  }

  int trap;
  cin >> trap;
  for (int i = 0; i < trap; ++i) {
    cin >> pos.first >> pos.second;
    traps[pos.first][pos.second] = true;
  }

  int wall;
  cin >> wall;
  for (int i = 0; i < wall; ++i) {
    cin >> pos.first >> pos.second;
    walls[pos.first][pos.second] = true;
  }
}

string CliffWalkEnvironment::to_string() {
  string s = "";
  for (int i = 0; i < dimen.first; ++i) {
    for (int j = 0; j < dimen.second; ++j) {
      environment[i][j] = FLOOR;
      if (traps[i][j])
        environment[i][j] = TRAP;
      if (treasure == vector<double>{lexical_cast<double>(i),
            lexical_cast<double>(j)})
        environment[i][j] = TREASURE;
      if (player == vector<double>{lexical_cast<double>(i),
            lexical_cast<double>(j)})
        environment[i][j] = PLAYER;
      if (walls[i][j])
        environment[i][j] = WALL;
      s += environment[i][j];
    }

    s += "\n";
  }
  cout << player[0] << " " << player[1] << endl;
  s += "\n";
  return s;
}

bool CliffWalkEnvironment::isPermitted(State &state, Action &action) {
  // Final state
  if (state == treasure)
    return true;

  // Borders
  if ((action == UP && state.get(0) == 0) ||
      (action == DOWN && state.get(0) == (dimen.first - 1)) ||
      (action == LEFT && state.get(1) == 0) ||
      (action == RIGHT && state.get(1) == (dimen.second - 1)))
    return false;
  if ((action == UP &&
       walls[(state.get(0) - 1 < 0)
             ? 0 : state.get(0) - 1][state.get(1)]) ||
      (action == DOWN &&
       walls[(state.get(0) + 1 >= dimen.first - 1)
             ? dimen.first - 1: state.get(0) + 1][state.get(1)]) ||
      (action == RIGHT &&
       walls[state.get(0)][(state.get(1) + 1 >= dimen.second - 1)
                           ? dimen.second - 1 : state.get(1) + 1]) ||
      (action == LEFT &&
       walls[state.get(0)][(state.get(1) - 1 < 0)
                           ? 0 : state.get(1) - 1]))
    return false;
  return true;
}

double CliffWalkEnvironment::doAction(Action &action) {
  player = action + player;
  if (player == treasure)
    return 0;
  else if (traps[player[0]][player[1]])
    return -10;

  return -0.1;
}

State CliffWalkEnvironment::getState() {
  State state(player);
  return state;
}

bool CliffWalkEnvironment::isTerminalState() {
  return (player == treasure || traps[player[0]][player[1]]) ? true : false;
}

void CliffWalkEnvironment::initRandom() {

}

void CliffWalkEnvironment::init(State &state) {
  player = {state.get(0), state.get(1)};
}
