#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <string>
#include <utility>
#include <vector>
#include "action/action.h"
#include "state/state.h"
#include <string>

class Environment {
public:
  virtual double doAction(Action &action) = 0;
  virtual State getState() = 0;
  virtual bool isTerminalState() = 0;
  virtual void initRandom() = 0;
  virtual void init(State &state) = 0;
  virtual std::string to_string() = 0;
};

#endif /* ENVIRONMENT_H */
