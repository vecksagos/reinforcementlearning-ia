#include "state.h"
#include <boost/lexical_cast.hpp>

using namespace std;
using boost::lexical_cast;

State::State(vector<double> state) :
  state(state) { }

string State::to_string() const {
  string s = "{ ";
  for (auto a : state)
    s += lexical_cast<string>(a) + " ";
  s += "}";

  return s;
}

bool State::operator==(const State &rhs) const {
  return state == rhs.state;
}

bool State::operator==(const vector<double> &rhs) const {
  return state == rhs;
}

double State::get(int i) {
  return state.at(i);
}

bool State::operator<(const State &rhs) const {
  return state < rhs.state;
}
