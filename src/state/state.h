#ifndef STATE_H
#define STATE_H

#include <vector>
#include <string>

class State {
private:
  std::vector<double> state;
public:
  State(std::vector<double> state);
  std::string to_string() const;
  bool operator==(const State &rhs) const;
  bool operator==(const std::vector<double> &rhs) const;
  bool operator<(const State &rhs) const;
  double get(int i);
};

#endif /* STATE_H */
