#ifndef HASHVFUNCTION_H
#define HASHVFUNCTION_H

#include "vFunction.h"
#include "state/state.h"
#include <map>

class HashVFunction: public VFunction {
private:
  std::map<State, double> table;
public:
  double getValue(State &state);
  void setValue(State &state, double value);
  std::string to_string();
};

#endif /* HASHVFUNCTION_H */
