#include "hashVFunction.h"
#include <random>
#include <iostream>
#include <boost/lexical_cast.hpp>

using namespace std;
using boost::lexical_cast;

double HashVFunction::getValue(State &state) {
  auto s = table.find(state);
  if (s != table.end()) {
    return table[state];
  }

  random_device rd;
  mt19937 gen(rd());
  uniform_real_distribution<double> dis(0, 0.1);
  table.insert(make_pair(state, dis(gen)));
  return table[state];
}

void HashVFunction::setValue(State &state, double value) {
  table[state] = value;
}

string HashVFunction::to_string() {
  string s = "{\n";
  for (auto it = table.begin(); it != table.end(); ++it) {
    s += "State " + it->first.to_string() + " Value " +
      lexical_cast<string>(it->second) + "\n";
  }
  s += "\n}";
  return s;
}
