#ifndef VFUNCTION_H
#define VFUNCTION_H

#include <string>

class State;

class VFunction {
public:
  virtual double getValue(State &state) = 0;
  virtual void setValue(State &state, double value) = 0;
  virtual std::string to_string() = 0;
};

#endif /* VFUNCTION_H */
