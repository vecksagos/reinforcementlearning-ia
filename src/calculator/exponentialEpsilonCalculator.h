#ifndef EXPONENTIALEPSILONCALCULATOR_H
#define EXPONENTIALEPSILONCALCULATOR_H

#include "epsilonCalculator.h"

class ExponentialEpsilonCalculator: public EpsilonCalculator {
private:
  double epsilon;
  int epoch;
public:
  ExponentialEpsilonCalculator(double epsilon);
  double calculate();
};

#endif /* EXPONENTIALEPSILONCALCULATOR_H */
