#ifndef STATICEPSILONCALCULATOR_H
#define STATICEPSILONCALCULATOR_H

#include "epsilonCalculator.h"

class StaticEpsilonCalculator: public EpsilonCalculator {
private:
  double epsilon;

public:
  StaticEpsilonCalculator(double epsilon);
  double calculate();
};

#endif /* STATICEPSILONCALCULATOR_H */
