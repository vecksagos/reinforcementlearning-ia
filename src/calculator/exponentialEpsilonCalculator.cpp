#include "exponentialEpsilonCalculator.h"
#include <cmath>

using namespace std;
ExponentialEpsilonCalculator::ExponentialEpsilonCalculator(double epsilon) :
  epsilon(epsilon) {
  epoch = 0;
}

double ExponentialEpsilonCalculator::calculate() {
  ++epoch;
  return epsilon - (1 - exp(-(epoch)));
}
