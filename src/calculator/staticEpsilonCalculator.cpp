#include "staticEpsilonCalculator.h"

StaticEpsilonCalculator::StaticEpsilonCalculator(double epsilon) :
  epsilon(epsilon) {
}

double StaticEpsilonCalculator::calculate() {
  return epsilon;
}
