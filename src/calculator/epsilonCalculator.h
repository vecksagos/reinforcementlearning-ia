#ifndef EPSILONCALCULATOR_H
#define EPSILONCALCULATOR_H

class EpsilonCalculator {
public:
  virtual double calculate() = 0;
};

#endif /* EPSILONCALCULATOR_H */
