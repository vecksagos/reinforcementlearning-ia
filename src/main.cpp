#include <iostream>
#include "environment/cliffWalkEnvironment.h"
#include "experiment/experiment.h"
#include "policy/epsilonGreedyPolicy.h"
#include "agent/agent.h"
#include "qFunction/hashQFunction.h"
#include "calculator/exponentialEpsilonCalculator.h"
#include "calculator/staticEpsilonCalculator.h"

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  CliffWalkEnvironment env;
  HashQFunction q(&CliffWalkEnvironment::ACTIONSET);
  StaticEpsilonCalculator epsilon(0.2);
  EpsilonGreedyPolicy policy(q, CliffWalkEnvironment::ACTIONSET, epsilon,
                             Policy::Q);
  Agent agent(policy);

  Experiment experiment(agent, env);
  experiment.run();

  return 0;
}
