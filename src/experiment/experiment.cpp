#include "experiment.h"
#include <iostream>

using namespace std;

Experiment::Experiment(Agent &agent, Environment &environment, int epochs,
                       int steps) :
  agent(agent),
  environment(environment),
  epochs(epochs),
  steps(steps) { }

void Experiment::run() {
  cout << "Starting a new experiment" << endl;
  auto initial = environment.getState();
  double totalReward = 0;
  for (int i = 0; i < epochs; ++i) {
    double totalEpiReward = 0;
    environment.init(initial);
    cout << "Starting episode " << i + 1 << " of " << epochs << endl;
    auto state = environment.getState();
    auto action = agent.start(initial);
    cout << "Action choose " << action.to_string() << endl;
    for (int j = 0; j < steps; ++j) {
      cout << "Starting step " << j + 1 << " of " << steps << endl;
      cout << environment.to_string() << endl;
      auto reward = environment.doAction(action);
      totalEpiReward += reward;
      state = environment.getState();
      action = agent.nextStep(state, reward);
      cout << "Action choose " << action.to_string() << endl;
      if (environment.isTerminalState())
        break;
    }
    cout << "Reward " << totalEpiReward << endl;
    cout << "Finished the episode" << endl;
    cout << "-------------------------" << endl;
    totalReward += totalEpiReward;
  }

  cout << "Mean reward " << totalReward / epochs << endl;
  cout << "Experiment finished" << endl;
  //cout << agent.to_string() << endl;
}
