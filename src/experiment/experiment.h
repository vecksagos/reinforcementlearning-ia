#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "agent/agent.h"
#include "environment/environment.h"

class Experiment {
private:
  Agent agent;
  Environment &environment;
  int epochs;
  int steps;

public:
  Experiment(Agent &agent, Environment &environment, int epochs = 1000,
             int steps = 5000);
  void run();
};

#endif /* EXPERIMENT_H */
