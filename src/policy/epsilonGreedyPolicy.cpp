#include "epsilonGreedyPolicy.h"
#include <iostream>
#include <random>
#include <limits>
#include "qFunction/qFunction.h"
#include <iostream>

using namespace std;

EpsilonGreedyPolicy::EpsilonGreedyPolicy(QFunction &q, ActionSet &actionSet,
                                         EpsilonCalculator &epsilonCalculator,
                                         char mode) :
  epsilonCalculator(epsilonCalculator),
  actionSet(actionSet),
  q(q),
  mode(mode) { }

Action EpsilonGreedyPolicy::getAction(State &state) {
  ActionSet valid = *actionSet.getValidActions(state);
  Action action = getBestAction(state);
  random_device rd;
  mt19937 gen(rd());
  uniform_real_distribution<double> realDis(0, 1);
  if (realDis(gen) <= epsilonCalculator.calculate()) {
    uniform_int_distribution<int> intDis(0, valid.size() - 1);
    action = valid[intDis(gen)];
  }

  return action;
}

Action EpsilonGreedyPolicy::getBestAction(State &state) {
  ActionSet *valid = actionSet.getValidActions(state);
  double max = q.getValue(state, (*valid)[0]);
  double v = 0;
  ActionSet best;
  for (auto action : *valid) {
    v = q.getValue(state, action);
    if (max > v)
      continue;
    else if (max < v) {
      max = v;
      best.clear();
      best.push_back(action);
      continue;
    }
    best.push_back(action);
  }

  random_device rd;
  mt19937 gen(rd());
  uniform_int_distribution<int> dis(0, best.size() - 1);
  return best[dis(gen)];
}

void EpsilonGreedyPolicy::update(State &previousState, Action &previousAction,
                                 State &state, Action &action, double reward) {
  Action bestAction = action;
  if (mode == Policy::Q)
    bestAction = getBestAction(state);
  q.setValue(previousState, previousAction, q.getValue(previousState,
                                                       previousAction)
             + 0.2 * (reward + q.getValue(state, bestAction)
                      - q.getValue(previousState,
                                   previousAction)));
}

string EpsilonGreedyPolicy::to_string() {
  return q.to_string();
}
