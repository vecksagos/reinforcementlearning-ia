#ifndef EPSILONGREEDYPOLICY_H
#define EPSILONGREEDYPOLICY_H

#include "policy.h"
#include "action/actionSet.h"
#include <map>
#include "calculator/epsilonCalculator.h"
#include <string>

class QFunction;

class EpsilonGreedyPolicy: public Policy {
private:
  EpsilonCalculator &epsilonCalculator;
  std::map<State, Action> bestAction;
  std::map<State, std::vector<Action>> action;
  ActionSet &actionSet;
  QFunction &q;
  char mode;

public:
  EpsilonGreedyPolicy(QFunction &q, ActionSet &actionSet,
                      EpsilonCalculator &epsilonCalculator, char mode);
  Action getAction(State &state);
  Action getBestAction(State &state);
  void update(State &previousState, Action &previousAction, State &state,
              Action &action, double reward);
  std::string to_string();
};

#endif /* EPSILONGREEDYPOLICY_H */
