#ifndef POLICY_H
#define POLICY_H

#include "state/state.h"
#include "action/action.h"
#include <string>

class Policy {
public:
  const static char SARSA = 'S';
  const static char Q = 'Q';
  virtual Action getAction(State &state) = 0;
  virtual Action getBestAction(State &state) = 0;
  virtual void update(State &previousState, Action &previousAction, State &state,
                      Action &action, double reward) = 0;
  virtual std::string to_string() = 0;
};

#endif /* POLICY_H */
