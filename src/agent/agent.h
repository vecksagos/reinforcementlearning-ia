#ifndef AGENT_H
#define AGENT_H

#include "state/state.h"
#include "action/action.h"
#include "policy/policy.h"
#include <string>

class Agent {
private:
  Policy &policy;
  State previousState;
  Action previousAction;
public:
  Agent(Policy &policy, Action action = Action({0, 1}), State state = State({0, 1}));
  Action start(State &state);
  Action nextStep(State &state, double reward);
  std::string to_string();
};

#endif /* AGENT_H */
