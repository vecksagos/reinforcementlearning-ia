#include "agent.h"

using namespace std;

Agent::Agent(Policy &policy, Action action, State state) :
  policy(policy),
  previousState(state),
  previousAction(action) {
}

Action Agent::start(State &state) {
  previousState = state;
  auto action = policy.getAction(state);
  previousAction = action;
  return action;
}

Action Agent::nextStep(State &state, double reward) {
  auto action = policy.getAction(state);
  policy.update(previousState, previousAction, state, action, reward);
  previousAction = action;
  previousState = state;
  return action;
}

string Agent::to_string() {
  return policy.to_string();
}
