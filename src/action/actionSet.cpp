#include "actionSet.h"

using namespace std;

ActionSet::ActionSet() { }
ActionSet::ActionSet(vector<Action> actionSet){
  for (auto as : actionSet)
    push_back(Action(as));
}

ActionSet* ActionSet::getValidActions(State &state) {
  if (filter == NULL)
    return this;

  ActionSet *valid = new ActionSet();
  for (auto action : *this)
    if (filter->isPermitted(state, action))
      valid->push_back(action);

  return move(valid);
}

string ActionSet::to_string() {
  string s = "";
  for (auto action : *this)
    s += action.to_string() + "\n";

  return s;
}

void ActionSet::setFilter(ActionFilter *filter) {
  this->filter = filter;
}
