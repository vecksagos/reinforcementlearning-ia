#include "action.h"
#include <utility>
#include <boost/lexical_cast.hpp>

using namespace std;
using boost::lexical_cast;
Action::Action() { }
Action::Action(vector<double> action) :
  action(action) { }

string Action::to_string() const {
  string s = "{ ";
  for (auto a : action)
    s += lexical_cast<string>(a) + " ";

  s += "}";

  return s;
}

bool Action::operator==(const Action &rhs) const {
  return action == rhs.action;
}

bool Action::operator==(const vector<double> &rhs) const {
  return action == rhs;
}

Action& Action::operator=(const vector<double> &rhs) {
  action = move(rhs);
  return *this;
}

vector<double> Action::operator+(const vector<double> &rhs) {
  vector<double> ret;
  for (unsigned long i = 0; i < action.size(); ++i)
    ret.push_back(action[i] + rhs[i]);

  return ret;
}

bool Action::operator<(const Action &rhs) const {
  return action < rhs.action;
}
