#ifndef ACTIONFILTER_H
#define ACTIONFILTER_H

#include "state/state.h"
#include "action.h"

class ActionFilter {
public:
  virtual bool isPermitted(State &state, Action &action) = 0;
};

#endif /* ACTIONFILTER_H */
