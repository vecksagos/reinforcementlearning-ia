#ifndef ACTIONSET_H
#define ACTIONSET_H

#include <vector>
#include <utility>
#include <string>
#include "action.h"
#include "actionFilter.h"
#include "state/state.h"

class ActionSet: public std::vector<Action> {
private:
  ActionFilter *filter;
public:
  ActionSet();
  ActionSet(std::vector<Action> actionSet);
  ActionSet* getValidActions(State &state);
  void setFilter(ActionFilter *filter);
  std::string to_string();
};

#endif /* ACTIONSET_H */
