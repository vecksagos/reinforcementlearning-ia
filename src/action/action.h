#ifndef ACTION_H
#define ACTION_H

#include <vector>
#include <string>

class Action {
private:
  std::vector<double> action;
public:
  Action(std::vector<double> action);
  Action();
  std::string to_string() const;
  bool operator==(const Action &rhs) const;
  bool operator==(const std::vector<double> &rhs) const;
  Action& operator=(const std::vector<double> &rhs);
  bool operator<(const Action &rhs) const;
  std::vector<double> operator+(const std::vector<double> &action);
};

#endif /* ACTION_H */
