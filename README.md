# Reinforcement Learning
- A reinforcement learning api on cpp to pass me on class of IA at UFAL
- Use [teaching box](http://sourceforge.net/projects/teachingbox/) to know what to do

## Requirements
- cmake
- boost

## How to compile
- the main file will execute the cliff walk environment
- create a file with
 - first line, the dimension of the map
 - second line, the initial position of the player
 - third line, the goal position
 - fourth line, the number of trap on the map
 - i-ths lines, the positions of the traps at the map
 - i+1-th line, number of wall on the map
 - j-ths lines, the positions of the walls
- create a new directory *build*, and execute
```shell
cd build
cmake -DCMAKE_BUILD_TYPE=Release ../
./cliffwalkcode < ../map
```
if you want to use clang instead of gcc, use
```shell
CXX=clang++ cmake -DCMAKE_BUILD_TYPE=Release ../
```

## Exemple of the a map
3 5
0 0
0 4
3
0 1
0 2
0 3
0
